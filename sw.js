importScripts("https://storage.googleapis.com/workbox-cdn/releases/6.1.1/workbox-sw.js");
workbox.precaching.precacheAndRoute([
	{ url: "/", revision: "1" },
	{ url: "/index.html", revision: "1" },
	{ url: "/assets/js/app.js", revision: "1" },
	{ url: "https://api.mapbox.com/mapbox-gl-js/v2.0.1/mapbox-gl.js", revision: "1" },
	{ url: "https://api.mapbox.com/mapbox-gl-js/v2.0.1/mapbox-gl.css", revision: "1" },
	{
		url:
			"https://api.mapbox.com/styles/v1/mapbox/dark-v10?access_token=pk.eyJ1IjoieXVzaGFybmFkaSIsImEiOiJjazd2YThmNGgwcTM5M2xtODhvYXg0emtrIn0.mKu3Skdm9aJrFqaWwbK0OA",
		revision: "1",
	},
	{ url: "/manifest.json", revision: "1" },
	{ url: "/icon-192x192.png", revision: "1" },
	{ url: "/icon-384x384.png", revision: "1" },
	{ url: "/icon-256x256.png", revision: "1" },
	{ url: "/icon-512x512.png", revision: "1" },
]);

// workbox.routing.registerRoute(
//     new RegExp("assets/"),
//     workbox.strategies.staleWhileRevalidate()
//     );

// 2. images
workbox.routing.registerRoute(
	new RegExp(".(png|svg|jpg|jpeg|gif)$"),
	new workbox.strategies.CacheFirst({
		cacheName: "My-awesome-cache-Images",
		plugins: [
			new workbox.expiration.ExpirationPlugin({
				maxAgeSeconds: 60 * 60 * 24 * 7,
				maxEntries: 50,
				purgeOnQuotaError: true,
			}),
		],
	})
);
// 1. stylesheet
workbox.routing.registerRoute(
	new RegExp(".css$"),
	new workbox.strategies.CacheFirst({
		cacheName: "My-awesome-cache-Stylesheets",
		plugins: [
			new workbox.expiration.ExpirationPlugin({
				maxAgeSeconds: 60 * 60 * 24 * 7, // cache for one week
				maxEntries: 20, // only cache 20 request
				purgeOnQuotaError: true,
			}),
		],
	})
);

// Menyimpan cache untuk file font selama 1 tahun
workbox.routing.registerRoute(
	/^https:\/\/fonts\.gstatic\.com/,
	new workbox.strategies.CacheFirst({
		cacheName: "google-fonts-webfonts",
		plugins: [
			new workbox.cacheableResponse.CacheableResponsePlugin({
				statuses: [0, 200],
			}),
			new workbox.expiration.ExpirationPlugin({
				maxAgeSeconds: 60 * 60 * 24 * 365,
				maxEntries: 30,
			}),
		],
	})
);

workbox.routing.registerRoute(
	/^https:\/\/fonts\.googleapis\.com/,
	new workbox.strategies.CacheFirst({
		cacheName: "google-fonts-stylesheets",
		plugins: [
			new workbox.cacheableResponse.CacheableResponsePlugin({
				statuses: [0, 200],
			}),
			new workbox.expiration.ExpirationPlugin({
				maxAgeSeconds: 60 * 60 * 24 * 365,
				maxEntries: 30,
			}),
		],
	})
);
