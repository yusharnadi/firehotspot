var markers = [];
var radios = document.querySelectorAll('input[type=radio][name="periode"]');
var host = "https://hotspot.brin.go.id/";

// RADIO BUTTON HANDLER
radios.forEach((radio) => {
  radio.addEventListener("change", (e) => {
    getDefault(
      `${host}getHSSingkawang?class=hotspot&conf_lvl=low&enddate=&mode=pixel&name=Hotspot&startdate=&time=${e.target.value}&visibility=true`
    );
  });
});

function getByDate() {
  var s_date = document.getElementById("s_date");
  var s_iso = new Date(s_date.value);
  var e_date = document.getElementById("e_date");
  var e_iso = new Date(e_date.value);

  if (!s_date.value || !e_date.value) {
    return;
  }

  var date_api = `${host}getHSSingkawang?class=hotspot&conf_lvl=low&startdate=${s_iso.toISOString()}&enddate=${e_iso.toISOString()}&mode=pixel&name=Hotspot&time=usedate&visibility=true`;

  getDefault(date_api);

  radios.forEach((radio) => {
    radio.removeAttribute("checked");
  });
}

mapboxgl.accessToken =
  "pk.eyJ1IjoieXVzaGFybmFkaSIsImEiOiJjazd2YThmNGgwcTM5M2xtODhvYXg0emtrIn0.mKu3Skdm9aJrFqaWwbK0OA";

var map = new mapboxgl.Map({
  container: "map",
  style: "mapbox://styles/mapbox/dark-v10",
  center: [109.05333801516855, 0.907010054774986],
  zoom: 11,
});

var nav = new mapboxgl.NavigationControl();

map.addControl(nav, "top-right");

map.addControl(
  new mapboxgl.GeolocateControl({
    positionOptions: {
      enableHighAccuracy: true,
    },
    trackUserLocation: true,
  })
);

function getDefault(
  endpoint = `${host}getHSSingkawang?class=hotspot&conf_lvl=low&enddate=&mode=pixel&name=Hotspot&startdate=&time=last24h&visibility=true`
) {
  var tot = document.getElementById("total");
  var host_kominfo = "https://statistik.singkawangkota.go.id";
  tot.innerText = "Sedang mengambil data . . .";

  var target = {
    url: endpoint,
  };

  fetch(host_kominfo, {
    method: "post",
    mode: "cors",
    cache: "no-cache",
    credentials: "same-origin",
    headers: {
      "Content-Type": "application/json",
    },
    redirect: "follow",

    body: JSON.stringify(target),
  })
    .then((response) => response.json())
    .then((data) => {
      if (markers !== null) {
        for (var i = markers.length - 1; i >= 0; i--) {
          markers[i].remove();
        }
      }
      var tot_res =
        data.features == null || data.features === undefined
          ? 0
          : data.features.length;
      tot.innerText = "Total Hotspot terdeteksi: " + tot_res;

      if (tot_res == 0) {
        return null;
      }
      data.features.map((point) => {
        var popup = new mapboxgl.Popup({
          offset: 10,
        }).setHTML(
          `<h5>Hotspot ID # ${point.id}</h5><p>Satelit : ${point.properties.sat}</p><p>Koordinat : ${point.geometry.coordinates}</p><p>Tingkat Kepercayaan: ${point.properties.conf}</p><p>Waktu Terdeteksi : ${point.properties.date}</p><p>Kecamatan : ${point.properties.kec}</p>`
        );
        var el = document.createElement("div");
        el.className = "cctv";
        if (point.properties.conf == 7) {
          el.style.backgroundImage = 'url("assets/img/green.gif")';
        } else if (point.properties.conf == 8) {
          el.style.backgroundImage = 'url("assets/img/yellow.gif")';
        } else {
          el.style.backgroundImage = 'url("assets/img/red.gif")';
        }

        marker = new mapboxgl.Marker(el)
          .setLngLat([
            point.geometry.coordinates["0"],
            point.geometry.coordinates["1"],
          ])
          .addTo(map)
          .setPopup(popup); // sets a popup on this marker;
        markers.push(marker);
      });
    })
    .catch(function (error) {
      console.log(error);
    });
}

// call first data
getDefault();
